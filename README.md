
![enter image description here](https://gitlab.com/geekaboy/cicodegen/raw/master/assets/images/logo-h.png)
# Welcome to CI Code Generator
#### Installation 🚀
1. Download [Click](https://gitlab.com/geekaboy/cicodegen/-/archive/master/cicodegen-master.zip)
2.  Unzip the package.
3.  Upload the CICodeGen folders and files to your server. Normally the  _index.php_  file will be at your root.
4.  Open the  _appsystem/config/config.php_  file with a text editor and set your base URL. If you intend to use encryption or sessions, set your encryption key.
5.  open the  _appsystem/config/database.php_  file with a text editor and set your database settings.
6. Run project and login with Username: admin and Password : 1234


#### Info 📌
Version => beta 0.1  
Repo =>  [https://gitlab.com/geekaboy/cicodegen](https://gitlab.com/geekaboy/cicodegen)  
Developer => SS2SEK  
Contact => suchartsek.p@gmail.com

#### Dev. on 💻
Database => PostgreSQL 10.10, MySQL 5.7.26  
OS => Ubuntu 18.04.1 LTS  
HTTP Server => Apache2.2.34  
PHP => 7.2  
Framework => codeigniter 3  
#### Buy me a coffee :) 🍺☕💰
![enter image description here](https://gitlab.com/geekaboy/cicodegen/raw/master/assets/images/promptpay_small.png)

![enter image description here](https://gitlab.com/geekaboy/cicodegen/raw/master/assets/images/paypal-me-small.png)
https://paypal.me/ss2sek
