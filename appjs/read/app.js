$(document).ready(function() {
    // $('#db_table').change(get_build_form_view);sw
    $('#db_schema').change(get_table_list);
    getDBSelect();



});//End ready

function get_table_list(){
    if($(this).val() == ''){
        return false;
    }
    clear_join();
    var param = {
        table_schema:$(this).val()
    };
    var url = site_url+'getdatabase/get_table_list?';
    url+= $.param(param);

    $.getJSON(url, function(resp, textStatus) {
        var option_html ='<option value="">*** TABLE ***</option>';
        $.each(resp, function(index, data) {
            option_html += '<option value="'+data.table_schema+'.'+data.table_name+'">'+data.table_schema+'.'+data.table_name+'</option>';
        });
        $('#db_table').html(option_html);

        setDBSelect();
        getTableSelect();


    });


}

function get_build_form_view(){
    show_preload();
    $('#div_build_form').html('');
    $('#div_code_view').html('');
    if($('#db_table').val() == ''){
        hide_preload();
        return false;
    }
    var isJoin = $('#joinToggle').prop('checked');

    var joinList = [];
    if(isJoin){
        var divJoinList = $('.divJoin');
        $.each(divJoinList, function(i, el) {
            if( $(el).find('#joinType').val() != '' &&
                $(el).find('#tableJoin').val() != '' &&
                $(el).find('#colJoin').val() != '' &&
                $(el).find('#tableJoinWith').val() != '' &&
                $(el).find('#colJoinWith').val() != ''){

                joinList[i] = {
                    'joinType':$(el).find('#joinType').val(),
                    'tableJoin':$(el).find('#tableJoin').val(),
                    'colJoin':$(el).find('#colJoin').val(),
                    'operator':$(el).find('#operator').val(),
                    'tableJoinWith':$(el).find('#tableJoinWith').val(),
                    'colJoinWith':$(el).find('#colJoinWith').val(),
                };
            }

        });
    }

    var param = {
        table_name:$('#db_table').val(),
        isJoin:isJoin,
        joinList:joinList,
    };
    var url = site_url+'read/build_code_view?';
    url+= $.param(param);
    $('#div_build_form').load(url,function(){
        $('html, body').animate({
            scrollTop: 0
        }, 500, 'linear');
        $('#generateTabs li a[href="#buildView"]').tab('show');
        setTableSelect();
        get_devname();
        hide_preload();
    });

}

function generate() {
    show_preload();
    $('#div_code_view').html('');
    var form = $('input[name="input_form[]"]:checked');
    var search_list =  $('input[name="search_by[]"]:checked').map(function(){
        return $(this).val();
      }).get();

    var input_list = [];
    $.each(form, function(index, el) {
        var column_name = $(el).data('column-name');
        var arr = {
            'column_name':column_name,
            'label':$('#input_label_'+column_name).val(),
            'column_default':$(el).data('column-default'),
            'data_type':$(el).data('data-type')
        };
        input_list.push(arr);
    });

    //JOIN FUNCTION
    var isJoin = $('#joinToggle').prop('checked');
    var joinList = [];
    var searchJoinList = [];
    var joinSearchList = [];
    if(isJoin){
        joinSearchList =  $('input[name="join_search_by[]"]:checked').map(function(){
            return $(this).val();
          }).get();
        var joinTable = $('.joinTable');
        $.each(joinTable, function(index, joinElement) {
            var searchJoinList =  $(joinElement).find('input[name="join_search_by"]:checked').map(function(){
                    return $(this).val();
                }).get();
            var formJoin = $(joinElement).find('input[name="join_input_form"]:checked');
            var colJoinList = [];
            $.each(formJoin, function(index, el) {
                var column_name = $(el).data('column-name');
                var table_name = $(el).data('table-name');
                var spl = table_name.split(".");
                var talbe_name = spl[1];
                // join_label_districts_name_in_thai
                // console.log($('#join_label_'+talbe_name+'_'+column_name).val());
                var arr = {
                    'column_name':column_name,
                    'label':$('#join_label_'+talbe_name+'_'+column_name).val(),
                    'column_default':$(el).data('column-default'),
                    'data_type':$(el).data('data-type')
                };
                colJoinList.push(arr);
            });



            var arr = {
                'buildJoinType':$(joinElement).find('input[name="buildJoinType"]').val(),
                'buildJoinTableName':$(joinElement).find('input[name="buildJoinTableName"]').val(),
                'buildJoinColName':$(joinElement).find('input[name="buildJoinColName"]').val(),
                'buildJoinOperator':$(joinElement).find('input[name="buildJoinOperator"]').val(),
                'buildJoinTableJoinWith':$(joinElement).find('input[name="buildJoinTableJoinWith"]').val(),
                'buildColJoinWith':$(joinElement).find('input[name="buildColJoinWith"]').val(),
                'colJoinList':colJoinList,


            };
            joinList.push(arr);

        });//END EACH



    }
    var param = {
        db_table: $('#db_table').val(),
        limit_list:$('#limit_list').val(),
        developer_name:$('#developer_name').val(),
        input_list:input_list,
        search_list:search_list,
        isJoin : isJoin,
        JoinList:joinList,
        joinSearchList:joinSearchList,



    };
    // return false;
    var url = site_url+'read/generate';
    $('#div_code_view').load(url, param, function(data){

        $('html, body').animate({
            scrollTop: 0
        }, 500, 'linear');
        $('#generateTabs li a[href="#copyPaste"]').tab('show');
        get_devname();
        setDBSelect();
        hide_preload();
    });
}
