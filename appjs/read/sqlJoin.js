$(document).ready(function() {
    $('#joinToggle').change(function() {
        if($(this).prop('checked')){
            $('#btnJoinMore').show();
            getJoinForm();
        }else {
            clear_join();
        }

    })

    $('#btnJoinMore').click(getJoinForm);

});//END CLASS

function getJoinForm(){
    show_preload();
    var url = site_url+'getdatabase/join_form_view?';
    var param = {
        table_schema:$('#db_schema').val()
    };
    url+= $.param(param);
    $.get(url, function(res){
        $('#joinForm').append(res);
        hide_preload();
    });

}

function removeJoin(el){
    var divJoin = $(el).parents('.divJoin');
    divJoin.remove();
    if($('.divJoin').length == 0){
        $('#joinForm').html('');
        $('#div_build_form').html('');
        $('#div_code_view').html('');
        $('#joinToggle').bootstrapToggle('off');
    }

}

function getColumnJoin(el){
    show_preload();
    if($(el).val() == ''){ return false; }
    var param = { table_name:$(el).val() };
    var url = site_url+'getdatabase/get_column_list?';
    url+= $.param(param);

    $.getJSON(url, function(resp, textStatus) {
        // console.log(resp);
        var option_html ='<option value="">*** COLUMN ***</option>';
        $.each(resp, function(index, data) {
            option_html += '<option value="'+data.table_name+'.'+data.column_name+'">'+data.column_name+'</option>';
        });
        //Set option colJoin
        $(el).parents('.divJoin').find('#colJoin').html(option_html);
        hide_preload();

    });
}

function getColumnJoinWith(el){
    show_preload();
    if($(el).val() == ''){ return false; }
    var param = { table_name:$(el).val() };
    var url = site_url+'getdatabase/get_column_list?';
    url+= $.param(param);

    $.getJSON(url, function(resp, textStatus) {
        var option_html ='<option value="">*** COLUMN ***</option>';
        $.each(resp, function(index, data) {
            option_html += '<option value="'+data.table_name+'.'+data.column_name+'">'+data.column_name+'</option>';
        });
        //Set option colJoinWith
        $(el).parents('.divJoin').find('#colJoinWith').html(option_html)
        hide_preload();
    });
}

function clear_join(){
    $('#btnJoinMore').hide();
    $('#joinForm').html('');
    $('#div_build_form').html('');
    $('#div_code_view').html('');
    if($('#joinToggle').prop('checked')){
        $('#joinToggle').bootstrapToggle('off');
    }


}
