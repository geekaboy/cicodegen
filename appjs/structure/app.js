$(document).ready(function() {
    get_viewcode();
    $('#bootTheme').change(get_viewcode);

});//End ready

function get_viewcode(){
    show_preload();
    var param = {theme:$('#bootTheme').val()}
    var url = base_url+'structure/view_codeview?'+$.param(param);
    $('#div_view_codeview').load(url, function(){
        Prism.highlightAll();
        hide_preload();

    });
}
function preview(){
    var theme = $('#bootTheme').val();
    if(theme != 'default'){
        var url = 'https://bootswatch.com/'+theme;
        window.open(url, '_blank');
    }else{
        var url = 'https://getbootstrap.com/docs/4.3/examples/';
        window.open(url, '_blank');
    }
}

function download(){
    var theme = $('#bootTheme').val();
    if (theme != 'default'){
        var url = 'https://bootswatch.com/4/'+theme+'/bootstrap.min.css';
    }else{
        var url = 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css';

    }
    window.open(url, '_blank');
}
