<?php
//------------[Controller File name : Structure.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Structure extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $sess = $this->session->userdata();
        if (!isset($sess['is_login']) || $sess['is_login'] == false) redirect('login');
        $this->load->model('database_table_model', 'db_table');
    }

    public function index()
    {
        $this->main_view();

    }

    public function main_view()
    {

        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/prismjs/prism.css',
            'assets/plugins/prismjs/prism.js',
            'assets/plugins/jquery.serializeJSON/jquery.serializejson.min.js',
        );
        $data['appjs'] = array(
            'appjs/structure/app.js'
        );


        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('structure/main_view');
        $this->load->view('theme/footer');
    }

    public function view_codeview()
    {
        $get = $this->input->get(NULL, TRUE);

        $theme = $get['theme'];
        $data['theme'] = $theme;

        $this->load->view('structure/view_codeview', $data);
    }
}//END Class
