<?php
//------------[Controller File name : Home.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');
header("X-Frame-Options: ALLOW-FROM http://gitlab.com");
class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $sess = $this->session->userdata();
        if (!isset($sess['is_login']) || $sess['is_login']  == false) redirect('login');


    }

    private  $limit = 30;

    public function index(){



        $this->main_view();
    }

    public function main_view(){
        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/prismjs/prism.css',
            'assets/plugins/prismjs/prism.js',
        );
        $data['appjs'] = array();

        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('home/main_view');
        $this->load->view('theme/footer');
    }

}//end class
