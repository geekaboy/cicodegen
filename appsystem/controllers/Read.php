<?php
//------------[Controller File name : Read.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Read extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $sess = $this->session->userdata();
        if (!isset($sess['is_login']) || $sess['is_login'] == false) redirect('login');
        $this->load->model('database_table_model','db_table');

    }

    public function index()
    {
        $this->main_view();
    }

    public function main_view()
    {
        //@Plugin & @Appjs
        $data['plugin'] = array(
            'assets/plugins/prismjs/prism.css',
            'assets/plugins/prismjs/prism.js',
            'assets/plugins/jquery.serializeJSON/jquery.serializejson.min.js',
            'assets/plugins/bootstrap4-toggle/css/bootstrap4-toggle.css',
            'assets/plugins/bootstrap4-toggle/js/bootstrap4-toggle.js'
        );
        $data['appjs'] = array(
            'appjs/read/app.js',
            'appjs/read/sqlJoin.js'
        );

        $data['schema_list'] = $this->db_table->get_table_schema();


        //@VIEW
        $this->load->view('theme/header', $data);
        $this->load->view('read/main_view');
        $this->load->view('theme/footer');

    }

    public function build_code_view()
    {
        $get = $this->input->get(NULL, TRUE);
        $data['table_name'] = $get['table_name'];
        $data['column_list'] = $this->db_table->get_column($get['table_name']);
        $data['join_colum_list'] = [];
        if(isset($get['joinList'])){

            foreach ($get['joinList'] as $key => $tbJoin) {

                $data['join_colum_list'][$key]['joinTableName'] = $tbJoin['tableJoin'];
                $data['join_colum_list'][$key]['joinType'] = $tbJoin['joinType'];
                $data['join_colum_list'][$key]['colJoin'] = $tbJoin['colJoin'];
                $data['join_colum_list'][$key]['operator'] = $tbJoin['operator'];
                $data['join_colum_list'][$key]['tableJoinWith'] = $tbJoin['tableJoinWith'];
                $data['join_colum_list'][$key]['colJoinWith'] = $tbJoin['colJoinWith'];
                $data['join_colum_list'][$key]['columList'] = $this->db_table->get_column($tbJoin['tableJoin']);
            }
        }

        $this->load->view('read/build_code_view', $data);



    }

    public function generate()
    {
        $post = $this->input->post(NULL, TRUE);
        $data['limit'] = $post['limit_list'];
        $data['search_list'] = (isset($post['search_list']))?$post['search_list']:[];
        $data['developer_name'] = $post['developer_name'];
        $data['input_list'] = $post['input_list'];
        $data['db_table'] = $post['db_table'];
        $data['isJoin'] = $post['isJoin'];
        $data['joinList'] = ($post['isJoin'] == 'true') ? $post['JoinList']:[];
        $data['joinSearchList'] = (isset($post['joinSearchList']))?$post['joinSearchList']:[];

        $this->load->view('read/code_view', $data);
    }

}//End class
