<?php
//------------[Controller File name : Getdatabase.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Getdatabase extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $sess = $this->session->userdata();
        if (!isset($sess['is_login']) || $sess['is_login'] == false) redirect('login');
        $this->load->model('database_table_model','db_table');

    }

    public function get_table_list(){
        $get = $this->input->get(NULL, TRUE);
        $table_list = $this->db_table->get_table($get['table_schema']);

        echo json_encode($table_list);

    }

    public function get_column_list(){
        $get = $this->input->get(NULL, TRUE);
        $column_list = $this->db_table->get_column($get['table_name']);

        echo json_encode($column_list);

    }

    public function join_form_view()
    {
        $get = $this->input->get(NULL, TRUE);
        $data['table_list'] = $this->db_table->get_table($get['table_schema']);
        $data['table_schema'] = $get['table_schema'];
        $this->load->view('read/join_form_view.php', $data);

    }

}//END Class
