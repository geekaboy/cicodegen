<?php
//------------[Controller File name : Configsystem.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Configsystem extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $sess = $this->session->userdata();
        if (!isset($sess['is_login']) || $sess['is_login'] == false) redirect('login');

    }

    public function index()
    {

    }

}//END CLASS
