    <div class="divJoin">
        <div class="row">
            <fieldset class="form-group col-lg-3 mb-0">
                <label for="joinTable">Join type</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <button class="btn btn-danger btn-remove" onclick="removeJoin(this)">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                    <select class="form-control" name="joinType" id="joinType">
                        <option value=""> *** JOIN TYPE ***</option>
                        <option value="LEFT"> LEFT JOIN</option>
                        <option value="RIGHT"> RIGHT JOIN</option>
                        <option value="INNER"> INNER JOIN</option>
                        <option value="FULL OUTER"> FULL OUTER JOIN</option>
                    </select>
                </div>

            </fieldset>
            <fieldset class="form-group col-lg-4 mb-0">
                <label for="joinTable">Table</label>
                <select class="form-control" name="tableJoin" id="tableJoin" onchange="getColumnJoin(this)">
                    <option value="">*** TABLE ***</option>
                <?php foreach ($table_list as $key => $table): ?>
                    <option value="<?php echo $table->table_schema.'.'.$table->table_name; ?>">
                        <?php echo $table->table_schema.'.'.$table->table_name; ?>
                    </option>
                <?php endforeach; ?>
                </select>

            </fieldset>
            <fieldset class="form-group col-lg-3">
                <label for="joinTable">Column</label>
                <select class="form-control" name="colJoin" id="colJoin">
                    <option value="">*** COLUMN ***</option>
                </select>
            </fieldset>

        </div>
        <div class="row">
            <fieldset class="form-group col-lg-1 offset-lg-3">
                <label for="joinTable">Operators</label>
                <select class="form-control" name="operator" id="operator" >
                    <option value="=">=</option>
                    <option value=">">></option>
                    <option value="<"><</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                    <option value="<>"><></option>
                </select>
            </fieldset>
            <fieldset class="form-group col-lg-3">
                <label for="joinTable">Table</label>
                <select class="form-control" name="tableJoinWith" id="tableJoinWith" onchange="getColumnJoinWith(this)">
                    <option value="">*** TABLE ***</option>
                <?php foreach ($table_list as $key => $table): ?>
                    <option value="<?php echo $table->table_schema.'.'.$table->table_name; ?>">
                        <?php echo $table->table_schema.'.'.$table->table_name; ?>
                    </option>
                <?php endforeach; ?>
                </select>
            </fieldset>
            <fieldset class="form-group col-lg-3">
                <label for="joinTable">Column</label>
                <select class="form-control" name="colJoinWith" id="colJoinWith">
                    <option value="">*** COLUMN ***</option>
                </select>
            </fieldset>
        </div><!--row-->
        <hr>
    </div>
