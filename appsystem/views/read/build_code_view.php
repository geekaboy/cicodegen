<div class="col-md-12">
  <h4><span class="badge badge-pill badge-success">2</span> Build view :</h4>
  <h5 class="ml-3"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Main table: <?php echo $table_name; ?></h5>
</div>
<fieldset class="form-group col-md-2 ml-3">
    <label for="limit_list">Limit list :</label>
    <input type="number" class="form-control" id="limit_list" placeholder="Ex. 50" value="50">
</fieldset>
<fieldset class="form-group col-md-3">
    <label for="developer_name">Developer name :</label>
    <div class="input-group">
        <input type="text" class="form-control" id="developer_name" placeholder="Ex. SS2SEK" value="">
        <div class="input-group-append">
            <button class="btn btn-primary" type="button" id="btn-dev-name" onclick="save_devname()">
                <i class="fa fa-save"></i> Save
            </button>
        </div>
    </div>
</fieldset>

<div class="col-md-12">
    <div class="table-responsive p-3">
      <table class="table table-hover table-bordered">
        <thead>
            <tr class="bg-info">
                <th width="30">#</th>
                <th class="text-center" width="50">Select</th>
                <th class="text-center" width="100">Search</th>
                <th class="text-center" width="200">Label</th>
                <th class="text-center" width="200">Column name</th>
                <th class="text-center" width="350">Data type</th>
                <th class="text-center">Default value</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i_row = 0;
            foreach ($column_list as $key => $column):
                $i_row++;
            ?>
                <tr>
                    <td><?php echo $i_row; ?></td>
                    <td class="text-center">
                        <input type="checkbox" name="input_form[]"
                            data-column-name="<?php echo $column->column_name; ?>"
                            data-column-default="<?php echo $column->column_default; ?>"
                            data-data-type="<?php echo $column->data_type; ?>" checked/>
                    </td>
                    <td class="text-center">
                        <input type="checkbox" name="search_by[]"
                            id="search_by_<?php echo $column->column_name; ?>" value="<?php echo $column->column_name; ?>"/>
                    </td>
                    <td>
                         <fieldset class="form-group">
                            <input type="text" class="form-control" id="input_label_<?php echo $column->column_name; ?>"
                                value="<?php echo $column->column_name; ?>">
                         </fieldset>
                    </td>
                    <td><?php echo $column->column_name; ?></td>
                    <td><?php echo $column->data_type ?></td>
                    <td><?php echo $column->column_default ?></td>
                </tr>
            <?php endforeach; ?>

        </tbody>
      </table>
    </div>
</div>

<div class="col-md-12 mt-5 ml-3 mr-3">
    <h5>Join list</h5>
    <?php
    if(empty($join_colum_list)){
        echo '<span class="text-info">*** No join ***</span>';
    }
    foreach ($join_colum_list as $key => $table):
        $ex = explode('.', $table['joinTableName']);
        $table_name = $ex[1];
    ?>

    <h5><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <?php echo $table['joinTableName']; ?></h5>
    <div class="table-responsive joinTable">
        <input type="hidden" name="buildJoinType" value="<?php echo $table['joinType']; ?>">
        <input type="hidden" name="buildJoinTableName" value="<?php echo $table['joinTableName']; ?>">
        <input type="hidden" name="buildJoinColName" value="<?php echo $table['colJoin']; ?>">
        <input type="hidden" name="buildJoinOperator" value="<?php echo $table['operator']; ?>">
        <input type="hidden" name="buildJoinTableJoinWith" value="<?php echo $table['tableJoinWith']; ?>">
        <input type="hidden" name="buildColJoinWith" value="<?php echo $table['colJoinWith']; ?>">
        <table class="table table-hover table-bordered">
            <thead>
                <tr class="bg-info">
                    <th width="30">#</th>
                    <th class="text-center" width="50">Select</th>
                    <th class="text-center" width="100">Search</th>
                    <th class="text-center" width="200">Label</th>
                    <th class="text-center" width="200">Column name</th>
                    <th class="text-center" width="350">Data type</th>
                    <th class="text-center">Default value</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i_row = 0;
                foreach ($table['columList'] as $key => $column):
                    $i_row++;
                    ?>
                    <tr>
                        <td><?php echo $i_row; ?></td>
                        <td class="text-center">
                            <input type="checkbox" name="join_input_form"
                            data-table-name="<?php echo $table['joinTableName']; ?>"
                            data-column-name="<?php echo $column->column_name; ?>"
                            data-column-default="<?php echo $column->column_default; ?>"
                            data-data-type="<?php echo $column->data_type; ?>"/>
                        </td>
                        <td class="text-center">
                            <input type="checkbox" name="join_search_by[]"
                            id="join_search_by_<?php echo $table_name.'_'.$column->column_name; ?>" value="<?php echo $table_name.'.'.$column->column_name; ?>"/>
                        </td>
                        <td>
                            <fieldset class="form-group">
                                <input type="text" class="form-control" name="join_label_<?php echo $table_name.'_'.$column->column_name; ?>"
                                id="join_label_<?php echo $table_name.'_'.$column->column_name; ?>" value="<?php echo $table_name.'_'.$column->column_name; ?>">
                            </fieldset>
                        </td>
                        <td><?php echo $column->column_name; ?></td>
                        <td><?php echo $column->data_type ?></td>
                        <td><?php echo $column->column_default ?></td>
                    </tr>
                <?php endforeach; ?>


            </tbody>
        </table>
    </div>
    <?php endforeach; ?>
</div>

<div class="col-md-12">
    <div class="text-center">
        <button type="button" class="btn btn-primary" onclick="generate()">
            Let's cook 🔥
        </button>
    </div>
    <hr>
</div>
