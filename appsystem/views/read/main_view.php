<style media="screen">
    td{
        padding: 5px !important;
        vertical-align:middle !important;
    }
    ol{
        columns: 2;
        -webkit-columns: 2;
        -moz-columns: 2;
    }
    #operator>option{
        text-align-last: center;
    }
</style>
<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CI:CODE</li>
        <li class="breadcrumb-item active">Read</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h3>
                            <i class="fa fa-code" aria-hidden="true"></i>
                            Read
                            <i class="fa fa-code" aria-hidden="true"></i>

                        </h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            <ul class="nav nav-tabs" id="generateTabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#selectDB" role="tab" aria-controls="selectDB">
                                        <i class="fa fa-database" aria-hidden="true"></i> Select database
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#buildView" role="tab" aria-controls="buildView">
                                        <i class="fa fa-wrench" aria-hidden="true"></i> Build view
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#copyPaste" role="tab" aria-controls="copyPaste">
                                        <i class="fa fa-clipboard" aria-hidden="true"></i> Copy & Paste
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="selectDB" role="tabpanel">
                                    <h4>
                                        <span class="badge badge-pill badge-success">1</span> Select database
                                    </h4>
                                    <div class="row mt-3 ml-3 mr-3">
                                        <fieldset class="form-group col-md-3 pl-0 ">

                                            <select class="form-control" name="db_schema" id="db_schema">
                                                <option value="">*** DATABASE ***</option>
                                            <?php foreach ($schema_list as $key => $schema): ?>
                                                <option value="<?php echo $schema->table_schema; ?>">
                                                    <?php echo $schema->table_schema; ?>
                                                </option>
                                            <?php endforeach; ?>
                                            </select>

                                        </fieldset>
                                        <fieldset class="form-group col-md-3">
                                            <select class="form-control" name="db_table" id="db_table">

                                            </select>

                                        </fieldset>
                                    </div>
                                    <div class="row border rounded p-2 m-0 ml-3 mr-3">
                                        <div class="col-md-12 mb-3 pl-0">
                                            <input type="checkbox" data-on="JOIN" data-off="NO JOIN"
                                                data-toggle="toggle" data-onstyle="success" id="joinToggle">
                                            <button type="button" class="btn btn-lg btn-primary" id="btnJoinMore" style="display:none;">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                        <div class="col-md-12 p-0 m-0" id="joinForm">
                                        </div>
                                        <div class="col-md-12 text-center">

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center mt-3">
                                            <button type="button" class="btn btn-primary" onclick="get_build_form_view(this)">
                                                🔥 Build
                                            </button>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane" id="buildView" role="tabpanel">
                                    <div class="row" id="div_build_form"></div>
                                </div>
                                <div class="tab-pane" id="copyPaste" role="tabpanel">
                                    <div class="row" id="div_code_view"></div>
                                </div>
                            </div>
                        </div>
                    </div>




                </div><!--card-body-->

            </div>
        </div>
    </div>
</main>
