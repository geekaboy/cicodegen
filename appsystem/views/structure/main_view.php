<style media="screen">
    td{
        padding: 5px !important;
        vertical-align:middle !important;
    }
    ol{
        columns: 2;
        -webkit-columns: 2;
        -moz-columns: 2;
    }
</style>
<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CI:CODE</li>
        <li class="breadcrumb-item active">Structure</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="text-center">
                        <h3>
                            <i class="fa fa-code" aria-hidden="true"></i>
                            Structure
                            <i class="fa fa-code" aria-hidden="true"></i>

                        </h3>
                    </div>
                    <div class="row mt-5" id="div_code_view">
                        <div class="col-md-12">
                            <small>CI:CODE => Copy & Past. Have a good day. :) </small>
                            <!-- tab -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" data-toggle="tab" href="#config">
                                        <i class="fa fa-wrench" aria-hidden="true"></i> Config
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#view" role="tab" aria-controls="view">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i> Theme
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#javascript" role="tab" aria-controls="javascript">
                                        <i class="fa fa-code" aria-hidden="true"></i> Javascript
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#helper" role="tab" aria-controls="home">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i> Helper
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#controler" role="tab" aria-controls="controler">
                                        <i class="fa fa-cog" aria-hidden="true"></i> Controller
                                    </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active show" id="config" role="tabpanel">
                                    <div id="div_intro_codeview">
                                        <?php $this->load->view('structure/intro_codeview'); ?>
                                    </div>
                                </div>
                                <div class="tab-pane" id="view" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <strong><i class="fa fa-cubes" aria-hidden="true"></i> Plugins list</strong>
                                            <ol>
                                                <li>
                                                    <a href="https://www.jsdelivr.com/package/npm/bootstrap?version=4.3.0" target="_blank">
                                                    Bootstrap v.4.3.1
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://fontawesome.com/v4.7.0/" target="_blank">
                                                    FontAwesome 4.7
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.jsdelivr.com/package/npm/jquery?version=3.3.1" target="_blank">
                                                    JQuery 3.3.1
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.jsdelivr.com/package/npm/jquery-serializejson?version=2.9.0" target="_blank">
                                                    SerializeJSON 2.9.0
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.jsdelivr.com/package/npm/sweetalert2?version=8.17.6" target="_blank">
                                                    SweetAlert2 8.17.6
                                                    </a>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                    <hr>

                                    <h5><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Select bootstrap theme</h5>
                                    <div class="row">
                                        <div class="col-md-4 pr-1">
                                            <fieldset class="form-group">
                                                <select class="form-control" name="bootTheme" id="bootTheme">
                                                    <option value="default">Default</option>
                                                    <option value="cerulean">Cerulean</option>
                                                    <option value="cosmo">Cosmo</option>
                                                    <option value="cyborg">Cyborg</option>
                                                    <option value="darkly">Darkly</option>
                                                    <option value="flatly">Flatly</option>
                                                    <option value="journal">Journal</option>
                                                    <option value="litera">Litera</option>
                                                    <option value="lumen">Lumen</option>
                                                    <option value="lux">Lux</option>
                                                    <option value="materia">Materia</option>
                                                    <option value="minty">Minty</option>
                                                    <option value="pulse">Pulse</option>
                                                    <option value="sandstone">Sandstone</option>
                                                    <option value="simplex">Simplex</option>
                                                    <option value="sketchy">Sketchy</option>
                                                    <option value="slate">Slate</option>
                                                    <option value="solar">Solar</option>
                                                    <option value="spacelab">Spacelab</option>
                                                    <option value="superhero">Superhero</option>
                                                    <option value="united">United</option>
                                                    <option value="yeti">Yeti</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-4 pl-0">
                                            <button type="button" class="btn btn-primary" id="btnPreview" onclick="preview()">
                                                <i class="fa fa-eye" aria-hidden="true"></i> Preview
                                            </button>
                                            <button type="button" class="btn btn-secondary" id="btnDownload" onclick="download()">
                                                <i class="fa fa-download" aria-hidden="true"></i> bootstrap.min.css
                                            </button>
                                        </div>
                                    </div>

                                    <div id="div_view_codeview"></div>
                                </div>
                                <div class="tab-pane" id="javascript" role="tabpanel">
                                    <div id="div_javascript_codeview"><?php $this->load->view('structure/javascript_codeview'); ?></div>
                                </div>
                                <div class="tab-pane" id="helper" role="tabpanel">
                                    <div id="div_helper_codeview">
                                        <?php $this->load->view('helper/plugin_codeview'); ?>
                                    </div>
                                </div>
                                <div class="tab-pane" id="controler" role="tabpanel">
                                    <div id="div_controller_codeview"><?php $this->load->view('structure/controller_codeview'); ?></div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div><!--card-body-->

            </div>
        </div>
    </div>
</main>

<?php $this->load->view('create/option_modal_view'); ?>
