<?php
$controller_codeview = '<?php
//------------[Controller File name : Upload.php ]----------------------//
if (!defined(\'BASEPATH\'))  exit(\'No direct script access allowed\');

class Upload extends CI_Controller {

    public function __construct(){
        parent::__construct();

    }

    public function index()
    {
        $this->main_view();

    }

    public function main_view()
    {
        //@Plugin & Appjs
		$data[\'plugin\'] = array(
            \'assets/plugins/dropzone/dist/dropzone.css\',
            \'assets/plugins/dropzone/dist/dropzone.js\'
        );
		$data[\'appjs\'] = array(
            \'appjs/demo/app.js\'
        );

		//@VIEW
		$this->load->view(\'theme/header\', $data);
		$this->load->view(\'upload/main_view.php\');
		$this->load->view(\'theme/footer\');

    }

    public function upload_process()
    {
        $post = $this->input->post(NULL, TRUE);
        $error_file = array();
        $complete_file = array();
        $filesCount = count($_FILES[\'files\'][\'name\']);
        for($i = 0; $i < $filesCount; $i++){
            $_FILES[\'file\'][\'name\']     = $_FILES[\'files\'][\'name\'][$i];
            $_FILES[\'file\'][\'type\']     = $_FILES[\'files\'][\'type\'][$i];
            $_FILES[\'file\'][\'tmp_name\'] = $_FILES[\'files\'][\'tmp_name\'][$i];
            $_FILES[\'file\'][\'error\']    = $_FILES[\'files\'][\'error\'][$i];
            $_FILES[\'file\'][\'size\']     = $_FILES[\'files\'][\'size\'][$i];

            $filename = md5($_FILES[\'file\'][\'name\'].date(\'YmdHis\'));
            $config[\'upload_path\']        = \'./DIR_STORE_YOUR_FILE/\';
            $config[\'file_name\']          = $filename;
            $config[\'file_ext_tolower\']   = true;
            $config[\'allowed_types\']      = \'gif|jpg|png\';
            $config[\'max_size\']           = 10240;//kb.

            $this->load->library(\'upload\', $config);
            if(!$this->upload->do_upload(\'file\')){
                $error_file[] = $_FILES[\'file\'][\'name\'] ;
            }else {
                $complete_file[] = $_FILES[\'file\'][\'name\'];
            }

        }
        echo json_encode(array(
            \'is_success\'=>TRUE,
            \'msg\'=>\'Upload success\',
            \'error\'=>$error_file,
            \'complete\'=>$complete_file
        ));

    }

}//END CLASS';

?>
<h5><i class="fa fa-dot-circle-o" aria-hidden="true"></i> application/controllers/Upload.php</h5>
<pre class="line-numbers language-php" ><code><?php echo htmlspecialchars($controller_codeview); ?></code></pre>
