<?php
$upload_html = '<main role="main" class="container">
    <div class="row mt-3">
      <div class="col-md-12">
          <div class="dropzone" id="div_upload" name="div_upload"></div>
          <div class="text-center mt-3">
              <button type="button" class="btn btn-primary" id="btn_upload" name="btn_upload">
                  <i class="fa fa-upload" aria-hidden="true"></i> Upload
              </button>
          </div>

      </div>
    </div>
</main>';
?>

<div class="row">
    <div class="col-md-4">
        <strong><i class="fa fa-cubes" aria-hidden="true"></i> Plugins list</strong>
        <ol>
            <li>
                <a href="https://www.jsdelivr.com/package/npm/dropzone?version=5.5.1" target="_blank">
                Dropzone v.5.5.1
                </a>
            </li>
        </ol>
    </div>
</div>
<hr>
<h5><i class="fa fa-dot-circle-o" aria-hidden="true"></i> views/upload/main_view.php</h5>
<pre class="line-numbers language-html" ><code><?php echo htmlspecialchars($upload_html); ?></code></pre>
