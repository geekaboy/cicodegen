<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">CI:CODE</li>
        <li class="breadcrumb-item active">Home</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <img src="<?php echo base_url('assets/images/sys_logo.png'); ?>" class="img" width="350" />

                            </div>
                            <div class="text-center mt-3">
                                <h4><u><i class="fa fa-info-circle" aria-hidden="true"></i> Info</u> </h4>
                                Version => 1.5<br>
                                Update lastest => 09-05-2020<br>
                                Repo => <a href="https://gitlab.com/geekaboy/cicodegen" target="_blank">https://gitlab.com/geekaboy/cicodegen</a><br>
                                Developer => @SEK<br>
                                Contact => suchartsek.p@gmail.com<br>

                            </div>
                            <div class="text-center mt-3">
                                <h4><u><i class="fa fa-code" aria-hidden="true"></i> Dev. on</u> </h4>
                                Database => PostgreSQL 10.10, MySQL 5.7.26 <br>
                                OS => Ubuntu 18.04.1 LTS<br>
                                HTTP Server => Apache2.2.34 <br>
                                PHP => 7.2<br>
                                Framework => Codeigniter 3<br>
                            </div>
                            <div class="text-center mt-3">
                                <h4><u><i class="fa fa-coffee" aria-hidden="true"></i> Buy me a coffee :)</u> </h4>
                                <div class="row justify-content-center">
                                  <div class="col-md-2 text-center">
                                    <img src="<?php echo base_url('assets/images/promptpay.png'); ?>" width="150" alt="">
                                  </div>
                                  <div class="col-md-2 text-center">
                                      <a href="https://paypal.me/ss2sek" target="_blank">
                                          <img src="<?php echo base_url('assets/images/paypal-me.png'); ?>" width="150" alt="">
                                      </a><br>
                                      <a href="https://paypal.me/ss2sek" target="_blank">https://paypal.me/ss2sek</a>
                                  </div>
                                </div>


                            </div>
                            <div class="row justify-content-center">
                                  <div class="col-md-6">
                                      <hr>
                                      <br><br>
                                      <p>
                                          <center><h4><i class="fa fa-file-text-o" aria-hidden="true"></i> Changelog</h4></center>
                                          <?php
                                          $change_log  = file_get_contents('https://gitlab.com/geekaboy/cicodegen/raw/master/CHANGELOG');
                                          echo $change_log;
                                          ?>
                                      </p>
                                  </div>
                            </div><!--row-->

                        </div><!--card-body-->
                    </div><!--card-->
                </div><!--col-md-12-->
            </div><!--row-->
        </div><!--animeted-->

    </div><!--container-->
</main>
